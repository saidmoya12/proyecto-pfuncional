package com.said.unitec.proyectofuncional;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {
	//private JSONObject dataBase = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		try {
			DatabaseManager.open(this.getApplicationContext()); //load Database from storage
			//JSONArray tikecs =  new DatabaseManager.selectFrom("tickets").toJSONArray();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void onLoginBtn(View view) {
        EditText eUsername = (EditText) this.findViewById(R.id.username);
        EditText ePassword = (EditText) this.findViewById(R.id.password);

        String username = eUsername.getText().toString();
        String password = ePassword.getText().toString();

        JSONObject currentUser = null, user = null;

        try {
            currentUser = new DatabaseManager //new query
                    .selectFrom("users")
                    .where("{username: " + username + "}").toJSONArray()
                    .getJSONObject(0);//fisrt result

            if(!currentUser.get("password").equals(password)){
                currentUser = null; //invalid current user
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(currentUser == null){
            eUsername.setError("Invalid username or password");
            ePassword.setError("Invalid username or password");
            return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        //intent.putExtra("dataBase", this.dataBase.toString());
        intent.putExtra("currentUser", currentUser.toString());
        startActivity(intent);

        //Toast.makeText(this.getApplicationContext(), user.toString(), Toast.LENGTH_LONG).show();
    }

		/*
		try {


			/*

			JSONArray users = this.dataBase.getJSONArray("users");

			for(int i = 0; i<users.length(); i++) {
				JSONObject user = null;
				user = users.getJSONObject(i);

				if(user.getString("username").equals(username)){
					if(user.getString("password").equals(password)){
						currentUser = user;
					}
					break;
				}
			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
    }

		if(currentUser == null){
			eUsername.setError("Invalid username or password");
			ePassword.setError("Invalid username or password");
			return;
		}

		/*
		Intent intent = new Intent(this, MainActivity.class);
		//intent.putExtra("dataBase", this.dataBase.toString());
		intent.putExtra("currentUser", currentUser.toString());
		startActivity(intent);*/
}
