package com.said.unitec.proyectofuncional;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditActivity extends AppCompatActivity {

    private JSONObject ticket;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			//dataBase = new JSONObject(this.getIntent().getExtras().getString("dataBase"));
			ticket = new JSONObject(this.getIntent().getExtras().getString("ticket"));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		setContentView(R.layout.activity_edit);

		try {
			((TextView) this.findViewById(R.id.eProjectName)).setText(ticket.getString("projectName").toUpperCase());
			((TextView) this.findViewById(R.id.eTikectCode)).setText(ticket.getString("code"));
			((TextView) this.findViewById(R.id.eExcecutor)).setText(ticket.getString("excecutor").toUpperCase());
			((TextView) this.findViewById(R.id.eDates)).setText(
					ticket.getString("startDate")+" | "+ticket.getString("endDate"));
		} catch (JSONException e) {
			e.printStackTrace();
		}

        renderTableContent();

		CheckBox finishButton = (CheckBox) this.findViewById(R.id.eFinished);

		final Context ctx = this;
		finishButton.setOnClickListener((v)->{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {

                String state = finishButton.isChecked() ? "TERMINADO" : "EJECUCION";
                ticket.remove("stateDate"); ticket.remove("state");
                ticket.put("stateDate", dateFormat.format(new Date())); //set new stateDate
                ticket.put("state", state);

                DatabaseManager.updateFrom("tickets", ticket, "{code: " + ticket.get("code") + "}");
                DatabaseManager.save(ctx.getApplicationContext());

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
                e.printStackTrace();
            }

            Intent main = new Intent();
			main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			setResult(Activity.RESULT_OK, main);
			finish();
		});
	}

    public void renderTableContent(){
        try {
            JSONArray locations = new DatabaseManager.selectFrom("locations").where("{ticket: "+ticket.get("code")+"}").toJSONArray();

            TableLayout locationsTbl = (TableLayout) findViewById(R.id.eLocations);

            //empty table (reset)
            if(locationsTbl.getChildCount()>1){
                locationsTbl.removeViews(1, locationsTbl.getChildCount()-1);
            }

            for(int i = 0; i<locations.length(); i++) {
                JSONObject item = null;
                TableRow row = new TableRow(this);
                Button b1 = new Button(this);
                TextView t1, t2;

                row.setTag(i);

                t1 = new TextView(this);
                t2 = new TextView(this);

                t1.setWidth(0);
                t2.setWidth(0);
                t1.setSingleLine(false);
                t2.setSingleLine(false);

                try {
                    item = locations.getJSONObject(i);
                    t1.setText(item.get("department").toString());
                    t2.setText(item.get("town").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                row.addView(t1);
                row.addView(t2);

                locationsTbl.addView(row);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}