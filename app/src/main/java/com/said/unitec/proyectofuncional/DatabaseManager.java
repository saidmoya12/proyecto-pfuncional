package com.said.unitec.proyectofuncional;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Administrator on 21/10/2015.
 */
public class DatabaseManager {

	private static JSONObject dataBase = null;

	public static JSONObject open(Context obj) throws IOException, JSONException {
		String loadedData;
		File db = new File(obj.getFilesDir().getAbsolutePath() + "/db.json");
        //db.delete(); //empty db
        if (db.exists()) {
			FileInputStream is = obj.openFileInput("db.json");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			loadedData = new String(buffer, "UTF-8");
		} else { //create if no exist /default empty entities
			FileOutputStream fos = obj.openFileOutput("db.json", Context.MODE_PRIVATE); //create
			loadedData = "{tickets: [], locations:[], users: [{name: \"Said Moya\", username: \"admin\", password: \"1234\"}]}";
			fos.write(loadedData.getBytes());
			fos.close();
		}

		DatabaseManager.dataBase = new JSONObject(loadedData);
		return DatabaseManager.dataBase;
	}

	/**
	 * Flush database
	 * @param context
	 * @throws IOException
	 */
	public static void save(Context context) throws IOException {
		FileOutputStream fos = context.openFileOutput("db.json", Context.MODE_PRIVATE); //create/update
		fos.write(DatabaseManager.dataBase.toString().getBytes());
		fos.close();
	}

	public static JSONObject getDatabase(){
		return DatabaseManager.dataBase;
	}

	public static class selectFrom {
		JSONArray entities;
		public selectFrom(String entityName) {
			try {
				entities = DatabaseManager.getDatabase().getJSONArray(entityName);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public selectFrom where(String condition){
			JSONArray result = new JSONArray();
			try {
				JSONObject conditional = new JSONObject(condition);

				for(int i = 0; i<entities.length(); i++){
					boolean match = false;
                    Iterator<String> keys = conditional.keys();
					while (keys.hasNext()){
						String key = keys.next();
						if(entities.getJSONObject(i).get(key).equals(conditional.get(key))){
							match = true;
						}
					}

					if(match){ result.put(entities.getJSONObject(i)); }
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			entities = result;
			return this;
		};

		public JSONArray toJSONArray(){
			return entities;
		}
	}

    public static void insertInto(String entityName, JSONArray values){
        try {
            for(int i = 0; i<values.length(); i++){
                insertInto(entityName, values.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void insertInto(String entityName, JSONObject value){
        JSONArray entities;
        try {
            entities = DatabaseManager.getDatabase().getJSONArray(entityName);
            entities.put(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void updateFrom(String entityName, JSONObject value, String condition){
        JSONArray entities = null;
        try {
            entities = DatabaseManager.getDatabase().getJSONArray(entityName);
            JSONObject conditional = new JSONObject(condition);

            for(int i = 0; i<entities.length(); i++){
                Iterator<String> keys = conditional.keys();
                boolean match = false;

                while (keys.hasNext()){
                    String key = keys.next();
                    if (entities.getJSONObject(i).get(key).equals(conditional.get(key))){
                        match = true;
                    }
                }

                if(match) {
                    entities.put(i, value);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}