package com.said.unitec.proyectofuncional;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

	private String netDPath;
    private JSONObject currentUser = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		try {
			currentUser = new JSONObject(this.getIntent().getExtras().getString("currentUser"));
			this.renderTable();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void renderTable() throws JSONException {
        JSONArray tickets = new DatabaseManager //new query
                .selectFrom("tickets").toJSONArray();

		final Context ctx = this;

		TableLayout mainTable = (TableLayout) findViewById(R.id.mainTable);
		//mainTable.removeAllViews();
		if(mainTable.getChildCount()>1){
			mainTable.removeViews(1, mainTable.getChildCount()-1);
		}

		for(int i = 0; i<tickets.length(); i++){
			JSONObject item = null;

			TableRow row = new TableRow(this);
			Button b1 = new AppCompatButton(this);

			TextView t1, t2, t3, t4;// = new TextView(this);

			t1 = new TextView(this);
			t2 = new TextView(this);
			t3 = new TextView(this);
			t4 = new TextView(this);

			b1.setWidth(0);
			b1.setMinWidth(0);
			b1.setTextSize(8);
			b1.setSingleLine(false);
			b1.setPadding(0, 0, 0, 0);
			t1.setWidth(0);
			t2.setWidth(0);
			t3.setWidth(0);
			t4.setWidth(0);

			t1.setSingleLine(false);
			t2.setSingleLine(false);
			t3.setSingleLine(false);
			t4.setSingleLine(false);

			try {
				item = tickets.getJSONObject(i);
				t1.setText(item.get("code").toString());
				t2.setText(item.get("excecutor").toString());
				t3.setText(item.get("state").toString());
				t4.setText(item.get("stateDate").toString());

				if(item.get("state").toString().equals("TERMINADO")){
					b1.setEnabled(false);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			b1.setText("Modificar");

			final JSONObject ticket = item;

			//LAMBDA Edit ticket
			b1.setOnClickListener((v) -> {
				Intent intent = new Intent(this, EditActivity.class);
				intent.putExtra("ticket", ticket.toString());
				startActivityForResult(intent, 2);
			});

			row.addView(b1);
			row.addView(t1);
			row.addView(t2);
			row.addView(t3);
			row.addView(t4);

			mainTable.addView(row);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode == Activity.RESULT_OK){
			try {
                Toast.makeText(this.getApplicationContext(),"Database updated", Toast.LENGTH_LONG).show();
				this.renderTable();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onNewBtn(View view){
		Intent intent = new Intent(this, RecordActivity.class);
		intent.putExtra("currentUser", currentUser.toString());
		startActivityForResult(intent, 2);
	}
}
