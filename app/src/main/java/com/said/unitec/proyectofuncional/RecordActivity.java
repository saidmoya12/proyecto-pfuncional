package com.said.unitec.proyectofuncional;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class RecordActivity extends AppCompatActivity {
	//private JSONObject dataBase = null;
	private JSONObject currentUser = null;

	private JSONObject departments = null;
	private JSONArray locations = null;

	private String ticketCode = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String loadedData = null;
		try {
			currentUser = new JSONObject(this.getIntent().getExtras().getString("currentUser"));

			InputStream is = this.getAssets().open("colombia.json");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			loadedData = new String(buffer, "UTF-8");

			departments = new JSONObject(loadedData);
			locations = new JSONArray();
		}catch (IOException ex) {
			ex.printStackTrace();
			return;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		setContentView(R.layout.activity_record);

		EditText startDate = (EditText) this.findViewById(R.id.rStartDate);
		EditText endDate = (EditText) this.findViewById(R.id.rEndDate);

		Context ctx = this;

		//LABMDA onClick event (JDK8)
		startDate.setOnClickListener((v) -> {
			new DateSelection(this, startDate).showDatePicker();
		});
		endDate.setOnClickListener((v) -> {
			new DateSelection(this, endDate).showDatePicker();
		});
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		this.populateDepartments();
	}

	private void populateDepartments(){
		List lables = new ArrayList<String>();
		Spinner spinner = (Spinner)this.findViewById(R.id.rDepartaments);

		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				populateTowns(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		Iterator<String> keys = departments.keys();
		while (keys.hasNext()){
			String key = keys.next();
			lables.add(key);
		}

		// Creating adapter for spinner
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, lables);

		// Drop down layout style
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		spinner.setAdapter(spinnerAdapter);
	}

	private void populateTowns(int index){
		if(this.departments==null) {
			return;
		}

		List lables = new ArrayList<String>();
		Spinner spinner = (Spinner)this.findViewById(R.id.rTowns);

		try {
			String dept = departments.names().getString(index);
			JSONArray towns = departments.getJSONArray(dept);

			for (int i=0; i < towns.length(); i++){
				lables.add(towns.getString(i));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Creating adapter for spinner
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item, lables);

		// Drop down layout style - list view with radio button
		spinnerAdapter
			.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(spinnerAdapter);
	}

	private void renderTableContent(){
		TableLayout locationsTbl = (TableLayout) findViewById(R.id.locations);

		//empty table (reset)
		if(locationsTbl.getChildCount()>1){
			locationsTbl.removeViews(1, locationsTbl.getChildCount()-1);
		}

		for(int i = 0; i<this.locations.length(); i++) {
			JSONObject item = null;
			TableRow row = new TableRow(this);
			Button b1 = new Button(this);
			TextView t1, t2;

			row.setTag(i);
			b1.setText("X");
			b1.setWidth(2);
			b1.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) {
					TableRow row = (TableRow) v.getParent();

					locations.remove((int) row.getTag());
					renderTableContent();
				}
			});

			t1 = new TextView(this);
			t2 = new TextView(this);

			t1.setWidth(0);
			t2.setWidth(0);
			t1.setSingleLine(false);
			t2.setSingleLine(false);

			try {
				item = this.locations.getJSONObject(i);
				t1.setText(item.get("department").toString());
				t2.setText(item.get("town").toString());

			} catch (JSONException e) {
				e.printStackTrace();
			}

			row.addView(b1);
			row.addView(t1);
			row.addView(t2);

			locationsTbl.addView(row);
		}
	}

	public void onAddLocation(View view){
		int iDepartament = ((Spinner) this.findViewById(R.id.rDepartaments)).getSelectedItemPosition();
		int iTown = ((Spinner) this.findViewById(R.id.rTowns)).getSelectedItemPosition();

		String departament = null, town = null;
		try {
			departament = departments.names().getString(iDepartament);
			town = departments.getJSONArray(departament).getString(iTown);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject location = new JSONObject();
		try {
			ticketCode = ("ACIP-"+ new Random().nextInt(999)+"-"+new Random().nextInt(999));
			location.put("ticket", ticketCode);
			location.put("department", departament);
			location.put("town", town);
			locations.put(location);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.renderTableContent();
	}

	public void onSaveBtn(View view){
		//JSONObject dataBase;
		JSONObject ticket = new JSONObject();

		try {
			//validate
			EditText projectName = (EditText) this.findViewById(R.id.rProjectName);
			EditText startDate = (EditText) this.findViewById(R.id.rStartDate);
			EditText endDate = (EditText) this.findViewById(R.id.rEndDate);

			boolean isValid = true;

			if(projectName.getText().toString().isEmpty()){
				isValid=false;
				projectName.setError("Proyecto es requeridos");
			}
			if(startDate.getText().toString().isEmpty()){
				isValid=false;
				startDate.setError("Fecha requerida");
			}if(endDate.getText().toString().isEmpty()){
				isValid=false;
				endDate.setError("Fecha requerida");
			}

			if(!isValid){
				return;
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			ticket.put("code", ticketCode);
			ticket.put("excecutor", currentUser.getString("name"));
			ticket.put("state", "EJECUCION");
			ticket.put("stateDate", dateFormat.format(new Date()));
			ticket.put("projectName", projectName.getText().toString());
			ticket.put("startDate", startDate.getText().toString());
			ticket.put("endDate", endDate.getText().toString());

            DatabaseManager.insertInto("tickets", ticket);
            DatabaseManager.insertInto("locations", locations);
			DatabaseManager.save(this.getApplicationContext());
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Intent main = new Intent();
		main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		setResult(Activity.RESULT_OK, main);
		finish();
	}
}

class DateSelection{

	private Context ctx;
	private EditText target;

	DateSelection(Context ctx, EditText target) {
		this.ctx = ctx;
		this.target = target;
	}

	public void showDatePicker(){
		Calendar mcurrentTime = Calendar.getInstance();

		int year, monthOfYear, dayOfMonth;

		if(target.getText().toString().isEmpty()){
			try {
				mcurrentTime.setTime((new SimpleDateFormat("yyyy-MM-dd")).parse(target.getText().toString()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		year = mcurrentTime.get(Calendar.YEAR);
		monthOfYear = mcurrentTime.get(Calendar.MONTH);
		dayOfMonth = mcurrentTime.get(Calendar.DAY_OF_MONTH);

		DatePickerDialog mDatePicker = new DatePickerDialog(this.ctx, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar cal = Calendar.getInstance();

				cal.set(Calendar.YEAR, year);
				cal.set(Calendar.MONTH, monthOfYear);
				cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

				target.setText((new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime()));
			}

		}, year, monthOfYear, dayOfMonth);
		mDatePicker.show();
	}
}